package com.example.tvshowmvvm.response;

import com.example.tvshowmvvm.model.TvShows;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TvShowsResponse {

    @SerializedName("page")
    private int page;

    @SerializedName("pages")
    private int total_pages;

    @SerializedName("tv_shows")
    private List<TvShows> tvShows;


    public int getPage() {
        return page;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public List<TvShows> getTvShows() {
        return tvShows;
    }
}
