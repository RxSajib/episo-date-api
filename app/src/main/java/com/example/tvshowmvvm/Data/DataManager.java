package com.example.tvshowmvvm.Data;

public class DataManager {

    public static final String BASEURL = "https://www.episodate.com/api/";
    public static final String TvShowID = "id";
    public static final String TvShowName = "TvShowName";
    public static final String TvShowCountry = "TvShowCountry";
    public static final String TvShowNetwork = "TvShowNetwork";
    public static final String TvStartDate = "TvStartDate";
}
