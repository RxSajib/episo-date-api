package com.example.tvshowmvvm.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TvShowDetails {

    @SerializedName("url")
    private String url;

    @SerializedName("rating")
    private String rating;

    @SerializedName("description")
    private String description;

    @SerializedName("start_date")
    private String start_date;


    @SerializedName("runtime")
    private String runtime;

    @SerializedName("image_path")
    private String image_path;

    @SerializedName("genres")
    private String[] genres;

    @SerializedName("pictures")
    private String[] pictures;

    @SerializedName("episodes")
    private List<Epsoid> epsoids;

    public String getUrl() {
        return url;
    }

    public String getDescription() {
        return description;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getImage_path() {
        return image_path;
    }

    public String[] getGenres() {
        return genres;
    }

    public String[] getPictures() {
        return pictures;
    }

    public List<Epsoid> getEpsoids() {
        return epsoids;
    }

    public String getRating() {
        return rating;
    }
}
