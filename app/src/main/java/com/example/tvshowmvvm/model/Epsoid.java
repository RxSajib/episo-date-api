package com.example.tvshowmvvm.model;

import com.google.gson.annotations.SerializedName;

public class Epsoid {

    @SerializedName("season")
    private String season;

    @SerializedName("episode")
    private String episode;

    @SerializedName("name")
    private String name;

    @SerializedName("air_date")
    private String relise_date;

    public String getSeason() {
        return season;
    }

    public String getEpisode() {
        return episode;
    }

    public String getName() {
        return name;
    }

    public String getRelise_date() {
        return relise_date;
    }
}
