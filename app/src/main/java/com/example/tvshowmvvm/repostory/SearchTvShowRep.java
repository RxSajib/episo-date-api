package com.example.tvshowmvvm.repostory;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.tvshowmvvm.model.TvShows;
import com.example.tvshowmvvm.network.ApiClint;
import com.example.tvshowmvvm.network.ApiServices;
import com.example.tvshowmvvm.response.TvShowsResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchTvShowRep {

    private ApiServices apiServices;

    public SearchTvShowRep(){
        apiServices = ApiClint.getRetrofit().create(ApiServices.class);
    }

    public LiveData<TvShowsResponse> getsearch(String qary, String page){
        MutableLiveData<TvShowsResponse> data = new MutableLiveData<>();

        apiServices.getsearch(qary, page).enqueue(new Callback<TvShowsResponse>() {
            @Override
            public void onResponse(@Nullable Call<TvShowsResponse> call,@Nullable Response<TvShowsResponse> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(@Nullable Call<TvShowsResponse> call,@Nullable Throwable t) {
                data.setValue(null);
            }
        });
        return data;
    }

}
