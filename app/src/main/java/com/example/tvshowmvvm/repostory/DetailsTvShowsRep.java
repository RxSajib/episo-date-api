package com.example.tvshowmvvm.repostory;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.tvshowmvvm.model.TvShowDetailsResponse;
import com.example.tvshowmvvm.network.ApiClint;
import com.example.tvshowmvvm.network.ApiServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsTvShowsRep {

    private ApiServices apiServices;

    public DetailsTvShowsRep(){
        apiServices = ApiClint.getRetrofit().create(ApiServices.class);
    }

    public LiveData<TvShowDetailsResponse> getdetails(String detailsid){
        MutableLiveData<TvShowDetailsResponse> data = new MutableLiveData<>();
        apiServices.get_tvshowdetails(detailsid).enqueue(new Callback<TvShowDetailsResponse>() {
            @Override
            public void onResponse(@Nullable Call<TvShowDetailsResponse> call,@Nullable Response<TvShowDetailsResponse> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(@Nullable Call<TvShowDetailsResponse> call,@Nullable Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }
}
