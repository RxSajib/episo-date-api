package com.example.tvshowmvvm.activites;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.tvshowmvvm.Adapter.Adapter;
import com.example.tvshowmvvm.R;
import com.example.tvshowmvvm.databinding.ActivityMainBinding;
import com.example.tvshowmvvm.databinding.ActivityMainBindingImpl;
import com.example.tvshowmvvm.model.TvShows;
import com.example.tvshowmvvm.response.TvShowsResponse;
import com.example.tvshowmvvm.viewmodel.TvshowViewmodel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TvshowViewmodel tvshowViewmodel;
    private Adapter adapter;
    private List<TvShows> tvShowsList = new ArrayList<>();
    private ActivityMainBinding activityMainBinding;

    private int currentpage = 1;
    private int totalpage = 1;
    private boolean Isopen = true;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         //setContentView(R.layout.activity_main);

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_black_87));
        }
        else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_black_87));
        }

        init_view();
        getdata();
        cntrol_fabbutton();
        click_fabbutton();
    }

    private void click_fabbutton(){
        activityMainBinding.FacSearchButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SearchPage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    private void cntrol_fabbutton(){
        activityMainBinding.FabPlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Isopen){
                    activityMainBinding.setFabWhitelist(true);
                    activityMainBinding.setWhitelisttext(true);
                    activityMainBinding.setFabSearch(true);
                    activityMainBinding.setSearchtext(true);
                    Isopen  = false;
                }else {
                    activityMainBinding.setFabWhitelist(false);
                    activityMainBinding.setWhitelisttext(false);
                    activityMainBinding.setFabSearch(false);
                    activityMainBinding.setSearchtext(false);
                    Isopen = true;
                }
            }
        });
    }

    private void init_view(){
        activityMainBinding.MostPolularViewID.setHasFixedSize(true);
        activityMainBinding.MostPolularViewID.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new Adapter(tvShowsList);
        activityMainBinding.MostPolularViewID.setAdapter(adapter);

        activityMainBinding.MostPolularViewID.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(!activityMainBinding.MostPolularViewID.canScrollVertically(1)){
                    if(currentpage <= totalpage){
                        currentpage += 1;
                        getdata();
                    }
                }
            }
        });
    }

    private void getdata(){

        activityMainBinding.setIsLoading(true);

        tvshowViewmodel = new ViewModelProvider(this).get(TvshowViewmodel.class);
        tvshowViewmodel.getmostpopulartvshows(currentpage).observe(this, new Observer<TvShowsResponse>() {
            @Override
            public void onChanged(TvShowsResponse tvShowsResponse) {

                totalpage = tvShowsResponse.getTotal_pages();
                int oldcount = tvShowsList.size();

                tvShowsList.addAll(tvShowsResponse.getTvShows());
                adapter.notifyItemRangeInserted(oldcount, tvShowsList.size());

                activityMainBinding.setIsLoading(false);
            }
        });

       /* tvshowViewmodel.getmostpopulartvshows(1).observe(this, new Observer<TvShowsResponse>() {
            @Override
            public void onChanged(TvShowsResponse tvShowsResponse) {
                //Toast.makeText(MainActivity.this, String.valueOf(tvShowsResponse.getTotal_pages()), Toast.LENGTH_SHORT).show();
            }
        });*/
    }
}