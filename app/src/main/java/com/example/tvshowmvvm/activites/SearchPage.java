package com.example.tvshowmvvm.activites;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tvshowmvvm.Adapter.Adapter;
import com.example.tvshowmvvm.R;
import com.example.tvshowmvvm.databinding.SearchPageBinding;
import com.example.tvshowmvvm.model.TvShows;
import com.example.tvshowmvvm.response.TvShowsResponse;
import com.example.tvshowmvvm.viewmodel.TvSearchViewmodel;

import java.util.ArrayList;
import java.util.List;

public class SearchPage extends AppCompatActivity {

    private SearchPageBinding pageBinding;
    private TvSearchViewmodel tvSearchViewmodel;
    private List<TvShows> tvShowsList = new ArrayList<>();
    private Adapter adapter;
    private String current_page = "1";


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageBinding = DataBindingUtil.setContentView(this,R.layout.search_page);

        setstatusbar_background();
        init_view();

       // getsearch(null, null);

    }

    private void getsearch(String epsoid_name, String page){
        tvSearchViewmodel = new ViewModelProvider(this).get(TvSearchViewmodel.class);
        pageBinding.setIsloading(true);
        tvSearchViewmodel.getsearch(epsoid_name, page).observe(this, new Observer<TvShowsResponse>() {
            @Override
            public void onChanged(TvShowsResponse tvShowsResponse) {
                tvShowsList.addAll(tvShowsResponse.getTvShows());
                adapter.notifyDataSetChanged();
                pageBinding.setIsloading(false);
            }
        });
    }

    private void init_view(){

        pageBinding.RecylerViewID.setHasFixedSize(true);
        pageBinding.RecylerViewID.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new Adapter(tvShowsList);
        pageBinding.RecylerViewID.setAdapter(adapter);

        pageBinding.BackButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        pageBinding.SearchInputID.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if(actionId == EditorInfo.IME_ACTION_DONE){
                    String program = pageBinding.SearchInputID.getText().toString().trim();
                    getsearch(program, current_page);

                }
                return true;
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setstatusbar_background(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_black_87));
        }
        else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_black_87));
        }
    }
}