package com.example.tvshowmvvm.activites;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EdgeEffect;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tvshowmvvm.Adapter.EpsioisAdapter;
import com.example.tvshowmvvm.Adapter.ImageSliderAdapter;
import com.example.tvshowmvvm.Data.DataManager;
import com.example.tvshowmvvm.R;
import com.example.tvshowmvvm.databinding.ActivityDetailsPageBinding;
import com.example.tvshowmvvm.model.TvShowDetailsResponse;
import com.example.tvshowmvvm.viewmodel.TvShowsDetailsViewmodel;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.time.Instant;

public class DetailsPage extends AppCompatActivity {

    private ActivityDetailsPageBinding activityDetailsPageBinding;
    private int MovieID;
    private TvShowsDetailsViewmodel tvShowsDetailsViewmodel;
    private BottomSheetDialog bottomSheetDialog;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityDetailsPageBinding = DataBindingUtil.setContentView(this, R.layout.activity_details_page);
        set_statusbar_background();


        init_view();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void set_statusbar_background(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    private void init_view(){
        activityDetailsPageBinding.BackButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        MovieID = getIntent().getIntExtra(DataManager.TvShowID, -1);
        tvShowsDetailsViewmodel = new ViewModelProvider(this).get(TvShowsDetailsViewmodel.class);


        get_data();
    }

    private void get_data(){
        activityDetailsPageBinding.setIsloading(true);
        tvShowsDetailsViewmodel.getdetailstvshows(String.valueOf(MovieID)).observe(this, new Observer<TvShowDetailsResponse>() {
            @Override
            public void onChanged(TvShowDetailsResponse tvShowDetailsResponse) {
                activityDetailsPageBinding.setIsloading(false);
                activityDetailsPageBinding.setProfileimage(tvShowDetailsResponse.getTvShowDetails().getImage_path());
                activityDetailsPageBinding.ViewPageID.setAdapter(new ImageSliderAdapter(tvShowDetailsResponse.getTvShowDetails().getPictures()));

                setdetails(tvShowDetailsResponse.getTvShowDetails().getDescription());
                activityDetailsPageBinding.setRating(tvShowDetailsResponse.getTvShowDetails().getRating());
                activityDetailsPageBinding.setRuntime(tvShowDetailsResponse.getTvShowDetails().getRuntime()+" Min");
                activityDetailsPageBinding.setGenres(tvShowDetailsResponse.getTvShowDetails().getGenres()[0]);



                activityDetailsPageBinding.WebSiteButtonID.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(tvShowDetailsResponse.getTvShowDetails().getUrl()));
                        startActivity(intent);
                    }
                });


                activityDetailsPageBinding.AllEpsiodButtonID.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog = new BottomSheetDialog(DetailsPage.this);
                        View view = LayoutInflater.from(DetailsPage.this).inflate(R.layout.epsoid_layout, null, false);

                        RecyclerView recyclerView = view.findViewById(R.id.EpsoidRecylerViewID);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        recyclerView.setAdapter(new EpsioisAdapter(tvShowDetailsResponse.getTvShowDetails().getEpsoids()));
                        TextView textView_title = view.findViewById(R.id.TitleID);
                        textView_title.setText(getIntent().getStringExtra(DataManager.TvShowName));

                        ImageView crossbutton = view.findViewById(R.id.CloseButtonID);
                        crossbutton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                bottomSheetDialog.dismiss();
                            }
                        });

                        bottomSheetDialog.setContentView(view);
                        bottomSheetDialog.show();
                    }
                });
            }
        });
    }

    private void setdetails(String details){
        activityDetailsPageBinding.setName(getIntent().getStringExtra(DataManager.TvShowName));
        activityDetailsPageBinding.setStartdate(getIntent().getStringExtra(DataManager.TvStartDate));
        activityDetailsPageBinding.setCountry(getIntent().getStringExtra(DataManager.TvShowCountry));
        activityDetailsPageBinding.setNetwork(getIntent().getStringExtra(DataManager.TvShowNetwork));
        activityDetailsPageBinding.setDetails(details);
    }
    
}