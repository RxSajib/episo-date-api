package com.example.tvshowmvvm.network;

import com.example.tvshowmvvm.model.TvShowDetailsResponse;
import com.example.tvshowmvvm.response.TvShowsResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiServices {

    @GET("most-popular")
    Call<TvShowsResponse> gettvshows(@Query("page") int page);

    @GET("show-details")
    Call<TvShowDetailsResponse> get_tvshowdetails(@Query("q") String showsid);

    @GET("search")
    Call<TvShowsResponse> getsearch(@Query("q") String query, @Query("page") String page);
}
