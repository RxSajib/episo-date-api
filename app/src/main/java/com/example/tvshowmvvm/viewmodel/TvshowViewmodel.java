package com.example.tvshowmvvm.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.tvshowmvvm.repostory.MostPopularTvshowsRep;
import com.example.tvshowmvvm.response.TvShowsResponse;

public class TvshowViewmodel extends ViewModel {

    private MostPopularTvshowsRep mostPopularTvshowsRep;
    public TvshowViewmodel(){
        mostPopularTvshowsRep = new MostPopularTvshowsRep();
    }

    public LiveData<TvShowsResponse> getmostpopulartvshows(int page){
        return mostPopularTvshowsRep.getmostpopulartvshowresponse(page);
    }
}
