package com.example.tvshowmvvm.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.example.tvshowmvvm.model.TvShowDetailsResponse;
import com.example.tvshowmvvm.repostory.DetailsTvShowsRep;

public class TvShowsDetailsViewmodel extends ViewModel {

    private DetailsTvShowsRep detailsTvShowsRep;

    public TvShowsDetailsViewmodel(){
        detailsTvShowsRep = new DetailsTvShowsRep();
    }

    public LiveData<TvShowDetailsResponse> getdetailstvshows(String showsid){
        return detailsTvShowsRep.getdetails(showsid);
    }

}
