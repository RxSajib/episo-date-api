package com.example.tvshowmvvm.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.tvshowmvvm.repostory.SearchTvShowRep;
import com.example.tvshowmvvm.response.TvShowsResponse;

public class TvSearchViewmodel extends ViewModel {

    private SearchTvShowRep searchTvShowRep;

    public TvSearchViewmodel(){
        searchTvShowRep = new SearchTvShowRep();
    }

    public LiveData<TvShowsResponse> getsearch(String quary, String page){
        return searchTvShowRep.getsearch(quary, page);
    }
}
