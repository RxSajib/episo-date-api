package com.example.tvshowmvvm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tvshowmvvm.R;
import com.example.tvshowmvvm.databinding.EpsoidsingleLayoutBinding;
import com.example.tvshowmvvm.databinding.EpsoidsingleLayoutBindingImpl;
import com.example.tvshowmvvm.model.Epsoid;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class EpsioisAdapter extends RecyclerView.Adapter<EpsioisAdapter.EpsoidHolder>{

    private List<Epsoid> epsoidList;
    private LayoutInflater layoutInflater;

    public EpsioisAdapter(List<Epsoid> epsoidList) {
        this.epsoidList = epsoidList;
    }

    @NonNull
    @NotNull
    @Override
    public EpsoidHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {

        layoutInflater = LayoutInflater.from(parent.getContext());
        EpsoidsingleLayoutBinding binding = EpsoidsingleLayoutBinding.inflate(layoutInflater, parent, false);
        return new EpsoidHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull EpsoidHolder holder, int position) {
        holder.epsoidsingleLayoutBinding.setAirDate(epsoidList.get(position).getRelise_date());

        holder.epsoidsingleLayoutBinding.setAirDate(epsoidList.get(position).getRelise_date());
        holder.epsoidsingleLayoutBinding.setName(epsoidList.get(position).getName());
        holder.epsoidsingleLayoutBinding.setEpsoidserial(epsoidList.get(position).getEpisode());
    }

    @Override
    public int getItemCount() {
        return epsoidList.size();
    }

    public static class EpsoidHolder extends RecyclerView.ViewHolder{

        private EpsoidsingleLayoutBinding epsoidsingleLayoutBinding;

        public EpsoidHolder(@NonNull @NotNull EpsoidsingleLayoutBinding epsoidsingleLayoutBinding) {
            super(epsoidsingleLayoutBinding.getRoot());

            this.epsoidsingleLayoutBinding = epsoidsingleLayoutBinding;

        }
    }
}
