package com.example.tvshowmvvm.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tvshowmvvm.Data.DataManager;
import com.example.tvshowmvvm.DataResponse.DataResponse;
import com.example.tvshowmvvm.activites.DetailsPage;
import com.example.tvshowmvvm.databinding.SingleIteamsBinding;
import com.example.tvshowmvvm.model.TvShows;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.MostPopularViewHolder>{

    private List<TvShows> tvShowsList;


    public Adapter(List<TvShows> tvShowsList) {
        this.tvShowsList = tvShowsList;

    }

    @NonNull
    @NotNull
    @Override
    public MostPopularViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        SingleIteamsBinding singleIteamsBinding = SingleIteamsBinding.inflate(inflater, parent,false);
        return new MostPopularViewHolder(singleIteamsBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MostPopularViewHolder holder, int position) {
        holder.singleIteamsBinding.setSetdata(tvShowsList.get(position));
        holder.singleIteamsBinding.executePendingBindings();

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), DetailsPage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(DataManager.TvShowID, tvShowsList.get(position).getId());
                intent.putExtra(DataManager.TvShowName, tvShowsList.get(position).getName());
                intent.putExtra(DataManager.TvShowNetwork, tvShowsList.get(position).getNetwork());
                intent.putExtra(DataManager.TvShowCountry, tvShowsList.get(position).getCountry());
                intent.putExtra(DataManager.TvStartDate, tvShowsList.get(position).getStart_date());
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(tvShowsList == null) {
            return 0;
        }
        return tvShowsList.size();
    }

    public static class MostPopularViewHolder extends RecyclerView.ViewHolder{

        private SingleIteamsBinding singleIteamsBinding;

        public MostPopularViewHolder(@NonNull @NotNull SingleIteamsBinding singleIteamsBinding) {
            super(singleIteamsBinding.getRoot());

            this.singleIteamsBinding = singleIteamsBinding;
        }

    }


}
