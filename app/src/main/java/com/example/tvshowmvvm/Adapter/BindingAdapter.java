package com.example.tvshowmvvm.Adapter;

import android.widget.ImageView;

import com.example.tvshowmvvm.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class BindingAdapter {

    @androidx.databinding.BindingAdapter("android:setimage")
    public static void SetimageUri(ImageView imageView, String url){
        try {
            imageView.setAlpha(0f);
            Picasso.get().load(url).placeholder(R.drawable.placeholder_image).into(imageView, new Callback() {
                @Override
                public void onSuccess() {
                    imageView.animate().alpha(1f).setDuration(300).start();
                }

                @Override
                public void onError(Exception e) {

                }
            });
        }catch (Exception r){

        }
    }
}
