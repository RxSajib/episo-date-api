package com.example.tvshowmvvm.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tvshowmvvm.databinding.SliderImageBinding;

import org.jetbrains.annotations.NotNull;

public class ImageSliderAdapter extends RecyclerView.Adapter<ImageSliderAdapter.ImageSliderViewHolder> {

    private String[] images;
    private LayoutInflater layoutInflater;

    public ImageSliderAdapter(String[] images) {
        this.images = images;
    }

    @NonNull
    @NotNull
    @Override
    public ImageSliderViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        layoutInflater  = LayoutInflater.from(parent.getContext());
        SliderImageBinding sliderImageBinding = SliderImageBinding.inflate(layoutInflater, parent, false);
        return new ImageSliderViewHolder(sliderImageBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ImageSliderViewHolder holder, int position) {
       holder.sliderImageBinding.setSliderimage(images[position]);
    }

    @Override
    public int getItemCount() {

        return images.length;
    }

    public static class ImageSliderViewHolder extends RecyclerView.ViewHolder{
        private SliderImageBinding sliderImageBinding;
        public ImageSliderViewHolder(@NonNull @NotNull SliderImageBinding sliderImageBinding) {
            super(sliderImageBinding.getRoot());
            this.sliderImageBinding = sliderImageBinding;
        }
    }
}
