package com.example.tvshowmvvm.DataResponse;

import com.example.tvshowmvvm.model.TvShows;

public interface DataResponse {

    void onTvShowClick(TvShows tvShows);
}
